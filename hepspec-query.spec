%define name hepspec-query
%define version 0.12

Summary: Tool to get per project metadata with performance indicators (hepspec)
Name: hepspec-query
Version: 0.12
Release: 1
Source0: %{name}-%{version}.tar.gz
Group: Development/Libraries
BuildRequires: python-devel
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch: noarch
Requires: python-keystoneclient
Requires: python-flask
Requires: python-flask-cache
Requires: python2-keystoneauth1
Requires: python-redis
Requires: python-werkzeug
License: GPL+

%description
Tool to get per project metadata with performance indicators (hepspec)

%prep
%setup -q 

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
install -d -m0755 %{buildroot}%{_datadir}/%{name}
%{__python} setup.py install --skip-build --install-scripts=%{_datadir}/hepspecquery --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_datadir}/hepspecquery/*
%{python_sitelib}/*

%changelog
* Tue Sep 1 2020 Ben Jones <b.jones@cern.ch> 0.12-1
- add disktype to results
* Fri May 8 2018 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.11-1
- Changed keystone api version
* Fri Apr 27 2018 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.10-1
- Added exception handling for unavailable Keystone API
