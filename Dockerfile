FROM gitlab-registry.cern.ch/linuxsupport/cs9-base
MAINTAINER Priyanshu Khandelwal <priyanshu.khandelwal@cern.ch>
ADD files/etc/yum.repos.d/* /etc/yum.repos.d/

RUN yum update -y

WORKDIR /app

RUN yum install -y python-keystoneclient python3-flask python3-keystoneauth1 python3-redis python3-werkzeug python3-pip python3-keystoneclient
RUN pip install flask-caching

COPY . /app

ENTRYPOINT [ "python3" ]
CMD [ "src/hepspecapp.py" ]

# CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]