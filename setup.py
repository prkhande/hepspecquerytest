#!/usr/bin/env python

from distutils.core import setup

setup(name='hepspec-query',
      version='0.2',
      description='Tool to get per project metadata with performance indicators (hepspec)',
      author='Ben Jones',
      author_email='ben.dylan.jones@cern.ch',
      scripts=['src/hepspecapp.py', 'src/hepspecquery.wsgi'],
      )

