#!/usr/bin/python

from flask import Flask
from flask import jsonify
from flask_caching import Cache
import os
import json


from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneauth1 import exceptions
from keystoneclient.v3 import client

app = Flask(__name__)
app.config["DEBUG"] = True
cache = Cache(app, config={"CACHE_TYPE": "redis", "CACHE_KEY_PREFIX": "hepspec-query",
                            "CACHE_REDIS_HOST": os.environ.get("REDIS_HOST", "127.0.0.1")})

def get_credentials():
    credentials = {}
    credentials['username'] = os.environ.get("USERNAME", "username")
    credentials['password'] = os.environ.get("PASSWORD", "password")
    credentials['project_name'] = os.environ.get("PROJECT_NAME", "project_name")
    credentials['auth_url'] = os.environ.get("AUTH_URL", "https://keystone.cern.ch/main/v3")
    credentials['user_domain_id'] = 'default'
    credentials['project_domain_id'] = 'default'
    return credentials

@app.route('/v1/batchtenants')
@cache.cached(timeout=28800)
def get_tenants():
    credentials = get_credentials()
    auth = v3.Password(**credentials)
    sess = session.Session(auth=auth)
    keystone = client.Client(session=sess)

    struct = {}

    try: 
        plist = keystone.projects.list(user=credentials['username'])
        # print(plist)
        for tenant in plist:
            if tenant.name not in struct:
                struct[tenant.name] = {}
                struct[tenant.id] = {}
            for key, value in tenant.to_dict().items():
                if key.startswith("hs"):
                    print(key)
                    print(value)
                    flavor = key.split('_')[1]
                    if '/' not in value:
                        configured = value
                        purchased = configured
                    else:
                        configured, purchased = value.split('/')

                    struct[tenant.name][flavor] = {'configured': configured, 'purchased': purchased}
                    struct[tenant.id][flavor] = {'configured': configured, 'purchased': purchased}
                elif key == "disktype":
                    struct[tenant.name]['disktype'] = value
                    struct[tenant.id]['disktype'] = value
            if 'disktype' not in struct[tenant.name]:
                struct[tenant.name]['disktype'] = 'ssd'
            if 'disktype' not in struct[tenant.id]:
                struct[tenant.id]['disktype'] = 'ssd'
    except exceptions.http.Unauthorized:
        struct["error"] = "Service unavailable. Please try again later."

    return jsonify(struct)

if __name__ == '__main__':
    app.run(debug=True)
